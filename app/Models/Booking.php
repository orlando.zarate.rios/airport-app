<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'booking';

    protected $fillable = [
        'code', 
        'seat',
        'flight_id', 
        'cat_class_id', 
        'user_id', 
        'status_id'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function flight() {
        return $this->belongsTo(Flight::class, 'flight_id', 'id');
    }

    public function class() {
        return $this->belongsTo(CatClass::class, 'cat_class_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function status() {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }
}
