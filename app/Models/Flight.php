<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flight extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code', 
        'cat_airline_id', 
        'plane_id', 
        'origin_cat_airport_id', 
        'destination_cat_airport_id',
        'date',
        'duration',
        'price'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $with = ['reservations', 'airline', 'plane', 'origin', 'destination'];

    public function reservations() {
        return $this->hasMany(Booking::class);
    }

    public function airline() {
        return $this->belongsTo(CatAirline::class, 'cat_airline_id', 'id');
    }

    public function plane() {
        return $this->belongsTo(Plane::class, 'plane_id', 'id');
    }

    public function origin() {
        return $this->belongsTo(CatAirport::class, 'origin_cat_airport_id', 'id');
    }

    public function destination() {
        return $this->belongsTo(CatAirport::class, 'destination_cat_airport_id', 'id');
    }

    public function scopeSearch($query, $search) {
        return $query->when(! empty ($search), function ($query) use ($search) {
            return $query->where(function($q) use ($search) {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('code', 'ilike', '%' . $search . '%');
                    $q->orWhere('date', 'ilike', '%' . $search . '%');
                }
            });
        });
    }
}
