<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatClass extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'price_multiplier'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
