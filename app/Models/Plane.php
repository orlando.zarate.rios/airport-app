<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plane extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $with = ['classes'];

    public function classes () {
        return $this->belongsToMany(CatClass::class, 'plane_has_classes', 'plane_id', 'cat_class_id');
    }
}
