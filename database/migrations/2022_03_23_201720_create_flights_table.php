<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('code');
            $table->unsignedBigInteger('cat_airline_id');
            $table->unsignedBigInteger('plane_id');
            $table->unsignedBigInteger('origin_cat_airport_id');
            $table->unsignedBigInteger('destination_cat_airport_id');
            $table->dateTime('date');
            $table->time('duration');
            $table->decimal('price', 11, 2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cat_airline_id')->references('id')->on('cat_airlines');
            $table->foreign('plane_id')->references('id')->on('planes');
            $table->foreign('origin_cat_airport_id')->references('id')->on('cat_airports');
            $table->foreign('destination_cat_airport_id')->references('id')->on('cat_airports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
