<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaneHasClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plane_has_classes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plane_id');
            $table->unsignedBigInteger('cat_class_id');
            $table->unsignedBigInteger('capactity');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('plane_id')->references('id')->on('planes');
            $table->foreign('cat_class_id')->references('id')->on('cat_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plane_has_class');
    }
}
