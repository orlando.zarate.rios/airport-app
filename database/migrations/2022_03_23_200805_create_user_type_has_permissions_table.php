<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTypeHasPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_type_has_permissions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cat_user_type_id');
            $table->unsignedBigInteger('cat_permission_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cat_user_type_id')->references('id')->on('cat_user_types');
            $table->foreign('cat_permission_id')->references('id')->on('cat_permissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_type_has_permissions');
    }
}
