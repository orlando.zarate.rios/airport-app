<?php

namespace Database\Seeders;

use App\Models\CatClass;
use Illuminate\Database\Seeder;

class CatClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatClass::create([
            'name' => 'ECONÓMICO',
            'price_multiplier' => 1.00
        ]);

        CatClass::create([
            'name' => 'NORMAL',
            'price_multiplier' => 1.35
        ]);

        CatClass::create([
            'name' => 'EJECUTIVO',
            'price_multiplier' => 1.45
        ]);
    }
}
