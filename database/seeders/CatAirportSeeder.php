<?php

namespace Database\Seeders;

use App\Models\CatAirport;
use Illuminate\Database\Seeder;

class CatAirportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatAirport::create([
            'name' => 'AEROPUERTO A'
        ]);

        CatAirport::create([
            'name' => 'AEROPUERTO B'
        ]);

        CatAirport::create([
            'name' => 'AEROPUERTO C'
        ]);
    }
}
