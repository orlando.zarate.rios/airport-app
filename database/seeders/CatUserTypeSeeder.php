<?php

namespace Database\Seeders;

use App\Models\CatUserType;
use Illuminate\Database\Seeder;

class CatUserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $basico = CatUserType::create([
            'name' => 'BÁSICO'
        ]);

        $basico->permissions()->createMany([
            ['name' => 'CONSULTAR'],
            ['name' => 'APARTAR']
        ]);

        $premium = CatUserType::create([
            'name' => 'PREMIUM'
        ]);

        $basico->permissions()->createMany([
            ['name' => 'CONSULTAR'],
            ['name' => 'APARTAR'],
            ['name' => 'COMPRAR']
        ]);
    }
}
