<?php

namespace Database\Seeders;

use App\Models\CatPermission;
use Illuminate\Database\Seeder;

class CatPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatPermission::create([
            'name' => 'CONSULTAR'
        ]);

        CatPermission::create([
            'name' => 'APARTAR'
        ]);

        CatPermission::create([
            'name' => 'COMPRAR'
        ]);
    }
}
