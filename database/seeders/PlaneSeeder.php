<?php

namespace Database\Seeders;

use App\Models\Plane;
use Illuminate\Database\Seeder;

class PlaneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plane::create([
            'name' => 'AVIÓN 1'
        ]);

        Plane::create([
            'name' => 'AVIÓN 2'
        ]);

        Plane::create([
            'name' => 'AVIÓN 3'
        ]);

        Plane::create([
            'name' => 'AVIÓN 4'
        ]);

        Plane::create([
            'name' => 'AVIÓN 5'
        ]);
    }
}
