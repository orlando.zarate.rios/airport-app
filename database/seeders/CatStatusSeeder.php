<?php

namespace Database\Seeders;

use App\Models\CatStatus;
use Illuminate\Database\Seeder;

class CatStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatStatus::create([
            'name' => 'RESERVADO'
        ]);

        CatStatus::create([
            'name' => 'COMPRADO'
        ]);

        CatStatus::create([
            'name' => 'CANCELADO'
        ]);
    }
}
