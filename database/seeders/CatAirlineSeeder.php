<?php

namespace Database\Seeders;

use App\Models\CatAirline;
use Illuminate\Database\Seeder;

class CatAirlineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatAirline::create([
            'name' => 'AEROLINEA 1'
        ]);

        CatAirline::create([
            'name' => 'AEROLINEA 2'
        ]);

        CatAirline::create([
            'name' => 'AEROLINEA 3'
        ]);
    }
}
