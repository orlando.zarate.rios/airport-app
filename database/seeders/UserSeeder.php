<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Usuario Básico',
            'email' => 'usuariobasico@mail.com',
            'password' => Hash::make('12345678'),
            'cat_user_type_id' => 1
        ]);

        User::create([
            'name' => 'Usuario Premium',
            'email' => 'usuariopremium@mail.com',
            'password' => Hash::make('12345678'),
            'cat_user_type_id' => 2
        ]);
        
    }
}
